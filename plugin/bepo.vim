if !empty(system("setxkbmap -print|grep bepo")) || !empty(system("swaymsg --type get_inputs | grep -i bepo"))
    " Remappe navigation page
    noremap <C-t> <C-f>
    noremap <C-s> <C-b>
    "" Half page
    noremap <C-PageDown> <C-d>
    noremap <C-PageUp> <C-u>
    noremap <CS-T> <C-d>
    noremap <CS-S> <C-u>

    " {W} -> [É]
    " ——————————
    " On remappe W sur É :
    noremap é w
    noremap É W
    " Corollaire: on remplace les text objects aw, aW, iw et iW
    " pour effacer/remplacer un mot quand on n’est pas au début (daé / laé).
    onoremap aé aw
    onoremap aÉ aW
    onoremap ié iw
    onoremap iÉ iW
    " Pour faciliter les manipulations de fenêtres, on utilise {W} comme un Ctrl+W :
    "noremap w <C-w>
    noremap W <C-w><C-w>
    noremap wo <C-w>o
    noremap wn <C-w>n
    noremap wv <C-w>v
    noremap wq <C-w>q
    noremap w_ <C-w>_
    noremap w<bar> <C-w><bar>
    noremap w] <C-w>]
    noremap w} <C-w>}
    noremap w= <C-w>=
    noremap wp :pc<cr>
    noremap w <C-w>

    " [HJKL] -> {CTSR}
    " ————————————————
    " {cr} = « gauche / droite »
    noremap c h
    noremap r l
    " {ts} = « haut / bas »
    noremap t j
    noremap s k
    " {CR} = « haut / bas de l'écran »
    noremap C H
    noremap R L
    " {TS} = « joindre / aide »
    noremap T J
    noremap S K
    " Corollaire : repli suivant / précédent
    noremap et zj
    noremap es zk

    " screen move
    noremap ee zz
    noremap eb zb
    noremap eé zt

    "folding
    noremap eo zo
    noremap ec zc
    noremap ef zf
    noremap eR zR
    noremap em zm

    " {HJKL} <- [CTSR]
    " ————————————————
    " {J} = « Jusqu'à »            (j = suivant, J = précédant)
    noremap j t
    noremap J T
    " {L} = « Change »             (l = attend un mvt, L = jusqu'à la fin de ligne)
    noremap l c
    noremap L C
    " {H} = « Remplace »           (h = un caractère slt, H = reste en « Remplace »)
    noremap h r
    noremap H R
    " {K} = « Substitue »          (k = caractère, K = ligne)
    noremap k s
    noremap K S
    " Corollaire : correction orthographique
    noremap ]k ]s
    noremap [k [s
     
    " Désambiguation de {g}
    " —————————————————————
    " ligne écran précédente / suivante (à l'intérieur d'une phrase)
    noremap gs gk
    noremap gt gj
    " onglet précédant / suivant
    noremap gb gT
    noremap gé gt
    " optionnel : {gB} / {gÉ} pour aller au premier / dernier onglet
    noremap gB :exe "silent! tabfirst"<CR>
    noremap gÉ :exe "silent! tablast"<CR>
    " optionnel : {g"} pour aller au début de la ligne écran
    noremap g" g0
     
    " <> en direct
    " ————————————
    noremap « <
    noremap » >
    noremap < «
    noremap > »
    inoremap « <
    inoremap » >
    inoremap < «
    inoremap > »
    vnoremap « <
    vnoremap » >
    vnoremap < «
    vnoremap > »
    onoremap « <
    onoremap » >
    onoremap < «
    onoremap > »
    cnoremap « <
    cnoremap » >
    cnoremap < «
    cnoremap > »
    lnoremap « <
    lnoremap » >
    lnoremap < «
    lnoremap > »
     
    " Remaper la navigation debut et fin de ligne
    " ———————————————————————————————————————————
    noremap ; ^
    noremap , $

    " Remaper la gestion des fenêtres
    " ———————————————————————————————
    noremap wt <C-w>j
    noremap ws <C-w>k
    noremap wc <C-w>h
    noremap wr <C-w>l
    noremap wT <C-w>J
    noremap wS <C-w>K
    noremap wC <C-w>H
    noremap wR <C-w>L
    noremap wd <C-w>c
    noremap wo <C-w>o
    " Open search file on eclim
    noremap wl :LocateFile<CR>
    noremap w<SPACE> :split<CR>
    noremap w<CR> :vsplit<CR>
    " Open tree
    noremap wz :NERDTreeToggle<cr>
    noremap wZ :NERDTreeFind<cr>
    noremap <C-w>z :NERDTreeToggle<cr>
    noremap <C-w>Z :NERDTreeFind<cr>

    " Tab fait un Esc, Maj+Tab fait un Tab
    inoremap <Tab> <Esc>
    inoremap <S-Tab> <Tab>

    " Même chose, mais en mode visuel
    vnoremap <Tab> <Esc>
    vnoremap <S-Tab> <Tab>
    lnoremap <Tab> <Esc>
    lnoremap <S-Tab> <Tab>


    " Save on à
    noremap à :w<CR>

    " NERDTree
    " ————————
    let g:NERDTreeMapChdir = 'H'
    let g:NERDTreeMapChdir = 'hd'
    let g:NERDTreeMapCWD = 'HD'
    let g:NERDTreeMapOpenInTab = 'j'
    let g:NERDTreeMapJumpLastChild = 'J'
    let g:NERDTreeMapOpenVSplit = 'k'
    let g:NERDTreeMapRefresh = 'l'
    let g:NERDTreeMapRefreshRoot = 'L'

    " nvim term
    tnoremap <C-t><C-q> <C-\><C-n>
    tnoremap <C-t><C-c> <C-\><C-n><C-w>h
    tnoremap <C-t><C-t> <C-\><C-n><C-w>j
    tnoremap <C-t><C-s> <C-\><C-n><C-w>k
    tnoremap <C-t><C-r> <C-\><C-n><C-w>l

    " nvim-completion-manager
    map èg :call phpactor#GotoDefinition()<CR>
endif
